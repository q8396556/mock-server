# Mock Server Project 

开发 模拟 接口 服务 项目  

## 简介  

项目 基于 express + mockjs  
实现 快速开发 接口 对接 api  


## 项目安装  

```bash
$ git clone https://gitee.com/nuokwan_backend_group/mock-server.git    # 下载项目源码 
$ cd mock-server       # 切换到项目目录 
$ npm install   # 安装项目依赖
$ npm i -g nodemon # 安装 nodemon
$ npm run dev  # 开发,项目启动 自动监听 修改 依赖 nodemon
```

## 项目说明  

> 目录说明
  
```
 apis    --- mock 接口文件
 bin     --- 项目上线启动脚本存目录
   -- www ---  服务启动脚本 
 config  --- 相关配置存储目录  
 public  --- 静态文件存储目录 
 view    --- html模板存储目录   
 app.js  --- 应用服务构造脚本
```

项目 在 遵从 express  开发模式 上 

增加了 自动 路由发现  , 在 config/app.js 配置中 apiDirs 指定目录中 

添加 对应 mock api js 文件 暴露对应信息对象即可(系统启动时会自动扫描加载)  

```js 
 const {dispatcher} = require('../core/controller');

 const handler = (req,res,next)=>{
        // xxx
 };
 const path = 'api/'; // 资源对外路由

 const prefix = '/'; // 公共前缀,默认 '/'

 // 暴露 对应 请求方法 (get,post,put,delete)
 const controller = dispatcher().get(prefix,handler);

 module.exports = { controller,path,prefix }
``` 

> config 目录中 配置说明 
 
 app.js http-server 相关配置
```js
const app = {
   appUrl : 'http://localhost', // 应用域名
   port : 80, // http-server 监听端口
   apiDirs : [path.join(__dirname, '../apis')], // api mock 目录
   staticDir : path.join(__dirname, '../public'), // 静态文件存储目录
   viewDir : path.join(__dirname, '../views'), // HTML模板存储目录
   loggerMode : 'dev', // 日志模式

   // app.set express 应用配置
   settings : {
      'view engine' : 'pug',
      'views': path.join(__dirname, '../views')
   },
   // app.use express 相关插件|中间件
   plugins : require('./plugins'),
}
``` 


 plugins.js 插件配置 
```js
const engine = require('../core/serverEngine');
const cookieParser = require('cookie-parser');
const apiCreateError = require('../core/middlewares/notFoundHandler');
const logger = require('morgan');

const createLogger = (app) => {
  return logger(app.siteconfig.loggerMode);
};

const errorRender = (err, req, res, next) => {
  if (next instanceof Function) {
    next();
  }
  console.log(res.body);
  if (res.body) {
    return;
  }
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
};

const createStatic = (app) => {
  return engine.static(app.siteconfig.staticDir);
};
const plugins = [
  engine.json(),
  cookieParser(),
  {register: createLogger}, // 动态注册插件 依赖 应用配置 
  engine.urlencoded({extended: false}),
  {register: createStatic},
  errorRender, // Function express 插件
  apiCreateError, // catch 404 and forward to error handler
];

``` 
## 启动代理 

为 本地开发 方便切换 mock 服务(方便本地开发 控制),提供了 proxy.js 工具脚本

public/javascripts/proxy.js  启动代理脚本   
   
```bash
$ #第一个参数 : 本地开发地址   
$ #第二个参数 : 远程真实api域名地址|远程 mock服务地址   
$ #第三个参数 : 本地代理 监听的端口 
$ node ./public/javascripts/proxy.js  http://localhost http://mockserver.word-server.com/mintee 80 
``` 

如果 使用 doclever 服务 mock ， 可是使用 net.js 启动本地代理服务 

public/javascripts/net.js  doclever 代理启动脚本  

```bash
$ node ./public/javascripts/net.js  http://localhost http://mockserver.word-server.com/mintee 80 
``` 
proxy.js 源于 net.js , net.js ES6 改造版  
去除相关 header doclever header 

## 项目依赖 

[lodash js 工具库](https://www.lodashjs.com/)    
[mockjs](http://mockjs.com)   
[express 4.x](https://expressjs.com/zh-cn/starter/installing.html)  
[cookie-parser](https://github.com/expressjs/cookie-parser)    
[morgan express logger ](https://github.com/expressjs/morgan)  
[pug HTML 模板引擎](https://pugjs.bootcss.com/)  

感谢开源平台 [doclever](http://doclever.cn)



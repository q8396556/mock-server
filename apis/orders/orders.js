const Mock = require('mockjs');
const Random = Mock.Random;
const {dispatcher} = require('../../core/controller');
const {pageStartId, pageKey, pageNextUrl, pageCurrentUrl,pagePrevUrl} = require('../../core/utils/page');

const prefix = '/';
const path = '/openapi/orders';

const handler = function (req, res, next) {

  let num = parseInt(req.query.pagSize || 10);
  let page = parseInt(req.query.pageNum || 1);

  let domain = 'http://' + req.hostname + req.baseUrl;
  let total = Random.integer(100,1000);

  let data = Mock.mock({
    [pageKey('list', num)]: [
      {
        ['id|+1']: pageStartId(page, num),// 属性 id 是一个自增数，起始值为 1，每次增 1
      }
    ],
    'meta': {
      'nextUrl': pageNextUrl(page, num,{domain}),
      'currentUrl': pageCurrentUrl(page, num, {domain}),
      'prevUrl': pagePrevUrl(page, num, {domain}),
      'page': page,
      'size': num,
      'total' : total,
    }
  });
  res.send(data);
};

const controller = dispatcher().get(prefix, handler);

module.exports = {
  controller,
  path,
  prefix,
};
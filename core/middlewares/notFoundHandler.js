
function createNotFoundMockApi(path,res){
   res.send({
        errcode : 1404,
        errmsg : '接口未定义开发',
        data : null,
   })
}
const handler =  (req,res,next) =>{
  next();
  createNotFoundMockApi(req.path,res);
};

module.exports = handler;